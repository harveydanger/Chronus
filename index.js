
const Chrouns = require('./dist/chronus').Chronus
const axios = require('axios')

/**
 * template must be of a Chronus.Chainable type (e.g. (input) => {return result})
 * @param {*} input - data
 */
const templateAxios = (input) =>{
    if(input.constructor === Object){
        input = input.data
    }
    console.log('\x1b[36m%s\x1b[0m', `↪ Axios call; passing ${input} for processing ⤵`);
    return (axios.post('http://127.0.0.1:9999/', {value: input}))
}

// For demo purposes, templateLocal represents local activity
const templateLocal = (input) => {
    if( input && input.constructor === Object){
        input = input.data
    }
    for(let i = 0; i < 10000; i ++){}
    console.log('\x1b[33m%s\x1b[0m',`↪ Local call; passing ${input} for processing ⤵`);
    r = input - 1
    return r
}

const st = [
    {other: [
        1,2,3,4
    ]},
    {other: [
        1,2,3,4
    ]},
    {other: [
        1,2,3,4
    ]},
    {other: [
        1,2,3,4
    ]}
]

async function Run(){
    let c = new Chrouns({verbose: true}); // verbose => show debug log 
    let chain = []
    for (let i = 0; i < st.length; i++){
        let ob = st[i];
        for(let j =0; j < ob.other.length; j++){
            let val = ob.other[j]
            let foo = (input)=>{
                return (axios.post('http://127.0.0.1:9999/', {value: val}))
            }
            chain.push(foo)
            if(j === ob.other.length -1){
                chain.push(null)
            }
        }
    }
    
    await c.PushToChainAll(chain) // await here is crucial, as if the chain function is not a function, an error is thrown
    let last = await c.Execute() // 0
    if(last.constructor === Object){
        last = last.data
    }

   
}
var figlet = require('figlet');
(async ()=>{
    return new Promise((resolve, reject) => {
        figlet('Chronus', (e,r) =>{
            resolve(r)
        })    
    });
    ;
})().then(async (data) =>{
    console.log(data)
}).then(()=>{
    Run()
})



