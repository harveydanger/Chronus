package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

// Request - Simple request structure for JSON parsing
type Request struct {
	Value interface{} `json:"value"`
}

func main() {

	http.HandleFunc("/", longWait)
	http.ListenAndServe(":9999", nil)

}

func longWait(w http.ResponseWriter, r *http.Request) {

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}

	var request Request

	json.Unmarshal(reqBody, &request)
	fmt.Println(r.RemoteAddr, "made a call", request.Value)

	fmt.Println("Pretend busy...")

	var min int32 = 2
	var max int32 = 10

	seed := rand.Int31n(max-min) + min

	for i := 0; i < int(seed); i++ {
		fmt.Print(".")
		time.Sleep(time.Second / 5)
	}
	fmt.Printf("\n%s\n", "done...")
	res := 0
	switch request.Value.(type) {
	case string:
		res, _ = (strconv.Atoi(request.Value.(string)))
	case int:
		res = request.Value.(int)
	case float64:
		res = int(request.Value.(float64))
	default:
		fmt.Println("Whhops, type is undefined", request.Value)
		fmt.Fprintf(w, "-1")
		return
	}

	res = res - 1

	fmt.Fprintf(w, strconv.Itoa(res))
}
