/*
    Chronus - plain / ajax call synchronisator
    Copyright (C) 2019 Vladimir Sukhov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/**
 * Chainable - predefined function signature that is used in a synchronous call
 */
export interface Chainable {
    (inp: any): any
}

export type Iteration = {
    id: string,
    fn: Chainable
}

interface Options {
    verbose: boolean
}

export class Chronus {
    isVerbose: boolean
    private chain: Array < Iteration >

        constructor(opt ? : Options) {
            if (!opt) {
                this.isVerbose = false
            } else {
                this.isVerbose = opt.verbose
            }
            this.chain = new Array < Iteration > ();
        }

    public PushToChain(item: Chainable): boolean {
        if (!this.VerifyChainable(item)){
            return false
        }
        this.chain.push({
            id: this.GenerateId(),
            fn: item
        })
        return true
    }

    public async PushToChainAll(items: Array < Chainable > ) {
        if (this.isVerbose) {
            console.log();
            console.log(`------------ Creating ${items.length} elements ------------`);
        }
        items.forEach(item => {
           if(!this.PushToChain(item)){
               throw "Item is not a Chainable function";
           }
        })
        if (this.isVerbose) {
            this.chain.forEach(e => {
                console.log(`Created: ${e.id}`);
            })
            console.log(`------------- Created ${this.chain.length} elements ------------`);
            console.log();
        }
    }

    public async Execute(): Promise < any > {
        if (this.chain.length === 0) {
            return false
        }
        let startArgument


        let next: Iteration | undefined = this.chain[0];
        if (this.isVerbose && next) {
            console.log(`↪ Iterating: ${next.id} ⤵`);
        }
        while (next) {
            startArgument = await next.fn(startArgument)
            next = this.GetNextCallable(next)
            if (this.isVerbose && next) {
                console.log("%s", `↪ Iterating: ${next.id} ⤵`);
            }
        }
        return startArgument
    }


    private VerifyChainable(item: any){
        if((item instanceof Function)){
            return true
        }
        return false
    }

    private GetNextCallable(current: Iteration): (Iteration | undefined) {
        for (let i = 0; i < this.chain.length; i++) {
            if (this.chain[i].id === current.id) {
                if (i + 1 > this.chain.length - 1) {
                    return undefined
                } else {
                    return this.chain[i + 1];
                }
            }
        }
    }

    private GenerateId() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0,
                v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}