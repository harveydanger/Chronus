# Description

Chronus - a small library that allows to chain local and ajax calls together, making all of them synchronous.
Library works on a chain paradigm, where each function inputs are dependant on previous function results.

***Important!: every chained function must conform to Chronus.Chainable interface (see chronus.ts for signature)***

# Installation

1. Run `npm install && npm run build`
2. Start dummy server `go run test/dummyserver.go`. Obviously get GO for your platform
3. Run `node index.js`

# Instructions

1. Add Chronus to your code `const Chrouns = require('./dist/chronus').Chronus`
2. Initialise Chronus `let c = new Chrouns({verbose: true}); // verbose => show debug log`
3. It is a good idea to use function templates based on a Chronus.Chainable interface, though templates are not necessary and lambdas | anonymous functions can be used
4. Create an array of functions that are Chainable and use `c.PushToChainAll(chain)`(or add functions by one `c.PushToChain(foo)`)
5. Execute and use a value of the last call if needed: `let last = await c.Execute()`

**Please bear in mind, that if you pass an object, you need to get a proper object key. e.g. axios returns object.data**
